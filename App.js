import { createAppContainer,createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
console.disableYellowBox = true;
import Homeview from './screens/Home';
import ScreenWith from './screens/ScreenWith';
import ScreenWith2 from './screens/ScreenWith2';


const AppNavigator = createStackNavigator({
  ScreenWith2:{
    screen:ScreenWith2
  },
  ScreenWith:{
   screen:ScreenWith
  }

});

const AppNavigatorone = createStackNavigator({

  Homeview:{
    screen:Homeview
  },
});

const App = createSwitchNavigator({
	AppNavigator : {
		screen: AppNavigator
	},

	AppNavigatorone:{
		screen:AppNavigatorone
	},
})

export default createAppContainer(App);
