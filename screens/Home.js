import React, { Component } from 'react';
import { StyleSheet,ActivityIndicator,View ,Dimensions,BackHandler, SafeAreaView} from 'react-native';
import { WebView } from "react-native-webview";
import GlobalStyles from './GlobalStyles';

export default class Home extends Component {
  static navigationOptions = { header:null } 


  constructor(props) {
    super(props);
    this.state = { visible: true };
        this.WEBVIEW_REF = React.createRef();

  }

  showSpinner() {
    console.log('Show Spinner');
    this.setState({ visible: true });
  }

  hideSpinner() {
    console.log('Hide Spinner');
    this.setState({ visible: false });
  }
componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

componentWillUnmount() {
  BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
}

handleBackButton = ()=>{
   this.WEBVIEW_REF.current.goBack();
   return true;
}

onNavigationStateChange(navState) {
  this.setState({
    canGoBack: navState.canGoBack
  });
}
  render() {
    return (
    	 <SafeAreaView  style={GlobalStyles.droidSafeArea}>
      <View
        style={this.state.visible === true ? styles.stylOld : styles.styleNew}>          
        {this.state.visible ? (
          <ActivityIndicator
            size="large"
            style={styles.ActivityIndicatorStyle}
          />

        ) : null}

        <WebView
          style={styles.WebViewStyle}
          //Loading URL
          source={{ uri: 'https://www.achhamall.com' }}
          //Enable Javascript support
          javaScriptEnabled={true}
          //For the Cache
          domStorageEnabled={true}
          //View to show while loading the webpage
          //Want to show the view or not
          //startInLoadingState={true}
           ref={this.WEBVIEW_REF}
          onNavigationStateChange={this.onNavigationStateChange.bind(this)}
          onLoadStart={() => this.showSpinner()}
          onLoad={() => this.hideSpinner()}
        />
      </View>
      </SafeAreaView>
    );
  }

}

const styles = StyleSheet.create({
  stylOld: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
        backgroundColor:'transparent',
  },
  styleNew: {
    flex: 1,
            backgroundColor:'transparent',

  },
  WebViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginTop:-2,
  },
  ActivityIndicatorStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor:'transparent',

    },
});